﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrainMovement : MonoBehaviour
{

	//Public movement variables
	public float MoveSpeed;

	//Private physics variables
	private Rigidbody2D TrainRigidBody;
	private Collider2D TrainCollider;


	// Use this for initialization
	void Start ()
	{
		//Getting the player's rigidbody
		TrainRigidBody = GetComponent<Rigidbody2D>();

		//Getting the player's collider
		TrainCollider = GetComponent<Collider2D>();
	}

	// Update is called once per frame
	void Update ()
	{
		// Forcing the train to always move forward
		TrainRigidBody.velocity = new Vector2(MoveSpeed, TrainRigidBody.velocity.y);
	}
}
