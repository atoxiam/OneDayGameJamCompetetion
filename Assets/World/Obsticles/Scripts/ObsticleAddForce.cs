﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObsticleAddForce : MonoBehaviour
{

	//Public object variables
	public static int score;
	public float thrust;
	public bool hasHitTrain;

	public bool scoredPoint;

	//private object variables
	private Rigidbody2D ObjectRigidBody;

	private bool isDeadObject;


	// Use this for initialization
	void Start ()

	{
		//Getting the object's rigidbody
		ObjectRigidBody = GetComponent<Rigidbody2D>();

		//Get is dead bool from player
		//GameObject playerScript = GameObject.Find("Player");
		//PlayerMovement player = playerScript.GetComponent<PlayerMovement>();
		//isDeadObject = player.isDead;
	}

	// Update is called once per frame
	void Update ()
	{
		//Checks if object has hit the train
		if(hasHitTrain == true)
		{
			//Adds force to the object after it hits the train
			ObjectRigidBody.AddRelativeForce(new Vector2(-1, 1) * thrust);

			//Checks if player is dead
			//if(isDeadObject == false)
			//{
				//Checks if you have already scored a point, if not adds 1 point
				if(scoredPoint == false)
				{
					score++;
					scoredPoint = true;
				}
			//}

		}

	}
void OnCollisionEnter2D(Collision2D col)
	{
		//Check if train has ran into the player if it has kill the player
		if(col.gameObject.tag == "Train")
		{
			hasHitTrain = true;

		}
	}
	 void OnGUI()
	{
		//Adds score to the gui once you have scored a point
     	GUILayout.Label( "Score = " + score);
	 }
}
