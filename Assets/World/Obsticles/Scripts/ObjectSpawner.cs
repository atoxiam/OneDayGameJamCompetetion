﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectSpawner : MonoBehaviour
{
	//Public spawner variables
	public GameObject[] Obsticles;
	public GameObject Object;
	public Transform spawnerPos;
	public int index;
	public float distanceBetween;
	public float maxDistanceBetween;
	public float minDistanceBetween;
	//Private spawner variables
	private float groundWidthSpawner;

	// Use this for initialization
	void Start ()

	{
		//Gets width of ground panels from ground generation script
		GameObject Generator = GameObject.Find("GroundGenerator");
		GroundGenerator groundGenerator = Generator.GetComponent<GroundGenerator>();
		groundWidthSpawner = groundGenerator.groundWidth;

	}

	// Update is called once per frame
	void Update ()
	{
			//Spawn ground panels once the postion of the platform is almost in view
		if (transform.position.x < spawnerPos.position.x)
		{
			//sets the index to a random int from 0 to the length of the obsticle array
			index = Random.Range (0, Obsticles.Length);
        	Object = Obsticles[index];


			//Sets the distance between objects to a random number between two given values
			distanceBetween = Random.Range(minDistanceBetween, maxDistanceBetween);

			//Sets the transform position for the obsticles to be spawn
			transform.position = new Vector3(transform.position.x + groundWidthSpawner + distanceBetween, transform.position.y, transform.position.z);

			//Spawns the obsticles
			Instantiate (Object, transform.position, transform.rotation);

		}


	}
}
