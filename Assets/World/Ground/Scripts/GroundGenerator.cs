﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundGenerator : MonoBehaviour
{
	//Public generator variables
	public GameObject Ground;
	public Transform generatorPos;
	public float distanceBetween;
	public float groundWidth;



	// Use this for initialization
	void Start ()
	{
		//Gets width of ground panels
		groundWidth = Ground.GetComponent<BoxCollider2D>().size.x;


	}

	// Update is called once per frame
	void Update ()
	{

		//Spawn ground panels once the postion of the platform is almost in view
		if (transform.position.x < generatorPos.position.x)
		{

			transform.position = new Vector3(transform.position.x + groundWidth + distanceBetween, transform.position.y, transform.position.z);
			Instantiate (Ground, transform.position, transform.rotation);

		}


	}
}
