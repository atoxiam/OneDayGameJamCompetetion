﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundDeletion : MonoBehaviour
{

	//Public Deletion Variables
	public GameObject groundDeletionPoint;

	// Use this for initialization
	void Start ()
	{

		//Get ground deletion point
		groundDeletionPoint = GameObject.Find("GroundDeletionPoint");

	}

	// Update is called once per frame
	void Update ()
	{

		if(transform.position.x < groundDeletionPoint.transform.position.x)
		{
			Destroy (gameObject);
            Destroy (GameObject.FindGameObjectWithTag("Obsticles"));
		}

	}
}
