﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

	//Public movement variables
	public float MoveSpeed;
	public float JumpForce;


	//Misc public variables
	public bool grounded;
	public bool isDead;
	public LayerMask WhatIsGround;

	//Private physics variables
	private Rigidbody2D PlayerRigidBody;
	private Collider2D PlayerCollider;

	//Private animation variables
		private Animator PlayerAnimator;


	// Use this for initialization
	void Start ()
	{
		//Getting the player's rigidbody
		PlayerRigidBody = GetComponent<Rigidbody2D>();

		//Getting the player's collider
		PlayerCollider = GetComponent<Collider2D>();

		//Getting player's animator
		PlayerAnimator = GetComponent<Animator>();

	}

	// Update is called once per frame
	void Update ()
	{
		// Check if player is touching the ground
		grounded = Physics2D.IsTouchingLayers(PlayerCollider, WhatIsGround);


		// Forcing the player to always move forward
		PlayerRigidBody.velocity = new Vector2(MoveSpeed, PlayerRigidBody.velocity.y);

		// Allows player to jump if they are touching the ground by using one of the two jump buttons
		if (grounded)
		{
			if (Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonDown(0))
			{
				PlayerRigidBody.velocity = new Vector2(PlayerRigidBody.velocity.x, JumpForce);
			}

		}
        // Play player animations
        PlayerAnimator.SetFloat("Speed", PlayerRigidBody.velocity.x);
        PlayerAnimator.SetBool("Grounded", grounded);

		if(isDead == true)
		{
			StartCoroutine(Death());
		}

	}
	void OnCollisionEnter2D(Collision2D col)
	{
		//Check if train has ran into the player if it has kill the player
		if(col.gameObject.tag == "Train")
		{
			isDead = true;
			Debug.Log("dead");
		}
	}
	IEnumerator Death()
	{
			PlayerAnimator.SetBool("IsDead", true);
			//Destroy
			Destroy (gameObject, 4);

			yield return new WaitForSeconds(2);

			Application.LoadLevel(1);
	}
}
