﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{

	//Public player variables
	public GameObject Train;


	//Private player follow variables
	private Vector3 lastTrainPosition;
	private float distanceToFollow;

	// Use this for initialization
	void Start ()
	{
		//Get the train gameObject
		Train = GameObject.FindGameObjectWithTag("Train");

		//Get the train's position
		lastTrainPosition = Train.transform.position;

	}


	// Update is called once per frame
	void Update ()
	{

		//Set the distance that the camera follows the train
		distanceToFollow = Train.transform.position.x - lastTrainPosition.x;
		transform.position = new Vector3(transform.position.x + distanceToFollow, transform.position.y, transform.position.z);
		lastTrainPosition = Train.transform.position;


	}
}
